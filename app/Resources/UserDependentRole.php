<?php
/**
 * Created by PhpStorm.
 * User: Іванка
 * Date: 19.11.2017
 * Time: 17:45
 */

namespace Resource;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserDependentRole implements RoleInterface
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getRole()
    {
        $allRoles = [];

        $session = new Session();
        if ($roles = $session->get('roles'))
        {
            foreach ($roles as $role) {
                $allRoles[] = 'ROLE_'.strtoupper($role);
            }
        }
        else
            $allRoles = ['ROLE_USER'];

        return $allRoles;
    }
}